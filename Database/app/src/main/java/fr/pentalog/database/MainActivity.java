package fr.pentalog.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PhoneBook phoneBook = new PhoneBook("New Phone Book");

        BoxStore boxStore = MyObjectBox.builder().androidContext(this).build();
        Box<PhoneBook> phoneBookBox = boxStore.boxFor(PhoneBook.class);
        Box<Contact> contactBox = boxStore.boxFor(Contact.class);

        long id = phoneBookBox.put(phoneBook);


        Contact contact1 = new Contact("George", "Butnaru", "0456567567", id);
        Contact contact2 = new Contact("George2", "Butnaru2", "0456567547", id);

        contactBox.put(contact1);
        contactBox.put(contact2);

        List<Contact> contacts = contactBox.query().equal(Contact_.firstName, "George").build().find();
        Log.d("db", "contacts " + contacts.size());

    }

    @NonNull
    private PhoneBook createPhoneBookWithContacts() {
        PhoneBook phoneBook = new PhoneBook("New Phone Book");

        BoxStore boxStore = MyObjectBox.builder().androidContext(this).build();
        Box<PhoneBook> phoneBookBox = boxStore.boxFor(PhoneBook.class);
        Box<Contact> contactBox = boxStore.boxFor(Contact.class);

        long id = phoneBookBox.put(phoneBook);


        Contact contact1 = new Contact("George", "Butnaru", "0456567567", id);
        Contact contact2 = new Contact("George2", "Butnaru2", "0456567547", id);

        contactBox.put(contact1);
        contactBox.put(contact2);
        return phoneBook;
    }

    private void simpleTest() {
        PhoneBook phoneBook = new PhoneBook("Work 2");

        BoxStore boxStore = MyObjectBox.builder().androidContext(this).build();
        Box<PhoneBook> phoneBookBox = boxStore.boxFor(PhoneBook.class);

        long id = phoneBookBox.put(phoneBook);

        PhoneBook dbPhoneBook = phoneBookBox.get(id);
        Log.d("db", "same object " + (phoneBook == dbPhoneBook));
    }
}
