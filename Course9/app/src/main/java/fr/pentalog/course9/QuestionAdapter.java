package fr.pentalog.course9;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Ionut Stirban on 11/05/2017.
 */

public class QuestionAdapter extends BaseAdapter {
    private List<Question> questions;
    private LayoutInflater inflater;
    private Context context;

    public QuestionAdapter(List<Question> questions, Context context) {
        this.questions = questions;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.question_item, parent, false);
        }
        final ImageView imgProfileImage = (ImageView) convertView.findViewById(R.id.img_profile_image);
        final TextView txtDisplayName = (TextView) convertView.findViewById(R.id.txt_display_name);
        final TextView txtViewCount = (TextView) convertView.findViewById(R.id.txt_view_count);
        final TextView txtTitle = (TextView) convertView.findViewById(R.id.txt_title);

        Question question = questions.get(position);

        Glide.with(context)
                .load(question.getOwner().getProfileImage())
                .placeholder(R.mipmap.ic_launcher)
                .into(imgProfileImage);

        txtDisplayName.setText(question.getOwner().getDisplayName());
        txtViewCount.setText("Views: " + question.getViewCount());
        txtTitle.setText(question.getTitle());

        return convertView;
    }
}
