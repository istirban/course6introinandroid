package fr.pentalog.course9;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ionut Stirban on 11/05/2017.
 */

public class Question {
    private String title;

    @SerializedName("view_count")
    private int viewCount;

    private Owner owner;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
