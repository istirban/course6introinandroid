package fr.pentalog.course9;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JsonParser jsonParser = new JsonParser();
        List<Question> questions = jsonParser.parseQuestions(this);

        QuestionAdapter adapter = new QuestionAdapter(questions, this);
        ListView lvQuestions = (ListView) findViewById(R.id.lv_questions);

        lvQuestions.setAdapter(adapter);
    }
}
