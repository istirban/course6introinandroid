package fr.pentalog.course9;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ionut Stirban on 11/05/2017.
 */

public class Owner {
    @SerializedName("profile_image")
    private String profileImage;

    @SerializedName("display_name")
    private String displayName;

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
